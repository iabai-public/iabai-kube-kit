# argo-starter-kit



## Description

Basic repository installing apps common for development clusters and testing environments. Typical first install by iab.ai

## Installation

### Prerequisites:
Have a kubernetes cluster running with ArgoCD installed.

Have ArgoCD cli installed in your workstation. [argocd](https://argo-cd.readthedocs.io/en/stable/cli_installation)

Clone the repository

`git clone git@gitlab.com:iabai-public/iabai-kube-kit.git`

`cd argo-starter-kit`

login your ArgoCD server, if needed proxy the ports or access through external URL

`kubectl port-forward svc/argocd-server -n argocd 8080:443`

```
ADMIN_USER="admin"
ADMIN_PASSWD="$(kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d)"

argocd login localhost:8080 --username $ADMIN_USER --password $ADMIN_PASSWD --insecure

```

create the app of apps in your argocd either by web interface or by cli:

```shell
argocd app create iabai-kube-kit \
    --dest-namespace argocd \
    --dest-server https://kubernetes.default.svc \
    --repo https://gitlab.com/iabai-public/iabai-kube-kit.git \
    --path clusters/dev/templates
```

then synchronize it:

`argocd app sync starter-kit-apps `




